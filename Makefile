.PHONY: check-tool-docker image retrieve-artifacts release

all: image retrieve-artifacts

VERSION := 1.0.0
DOCKER := $(shell command -v docker 2> /dev/null)
IMAGE_NAME := static-kata-container

check-tool-docker:
ifndef DOCKER
		$(error "`docker` is not available please install docker (https://docs.docker.com/install/)")
endif

image: check-tool-docker
	$(DOCKER) build -t $(IMAGE_NAME) .

retrieve-artifacts: check-tool-docker
	$(DOCKER) run --rm --entrypoint cat $(IMAGE_NAME) /kata-runtime > kata-runtime
	$(DOCKER) run --rm --entrypoint cat $(IMAGE_NAME) /kata-runtime.sha512 > kata-runtime.sha512

# The below target is only to be run on version (`vX.X.X`) branches -
# command runs with the expectation that the resulting binary will be downloaded over HTTPS from some source-control mechanism
release: check-tool-docker image retrieve-artifacts
	rm -rf Dockerfile	Makefile README.md Makefile
