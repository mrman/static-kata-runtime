FROM alpine:3.7

# Install dependencies
RUN apk add --update make openssh git go linux-headers alpine-sdk

# Retrieve kata-runtime source
RUN go get -d -u github.com/kata-containers/runtime || true # `go get` errors due to no Go files being present

# FIX: super fragile...
WORKDIR /root/go/src/github.com/kata-containers/runtime
RUN sed -i 's/go build/go build --ldflags '"'-linkmode external -extldflags "'"-static"'"'"'/' Makefile
RUN make -j
RUN mv kata-runtime /kata-runtime
RUN sha512sum /kata-runtime > /kata-runtime.sha512
