# `static-kata-runtime` #

A static build of `kata-runtime` for use on [Container Linux](https://coreos.com/os/docs/) distributions

This repository contains a `Dockerfile` for use with [`docker run`](https://docs.docker.com/engine/reference/commandline/run/).

# Requirements #

- [docker](https://docs.docker.com)

# Quickstart #

Pre-compiled binaries are kept in branches according to the version of [`kata/runtime`](https://github.com/kata-containers/runtime) that they match.
You can download them by checking/downloading the tags for repo (they should contain the binary and a checksum), or by using the links below:

| version  | download link                                                                                               | `kata-runtime` binary `sha512` checksum (also inside the archive)                                                                  |
|----------|-------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| `v1.0.0` | [download](https://gitlab.com/mrman/static-kata-runtime/-/archive/v1.0.0/static-kata-runtime-v1.0.0.tar.gz) | `6d364e0425763f334a89f53c2273f15f22574735667d083eaa1d7029b9def024d8e75c47982375d0ffd6278c8250c1ba7aa16c79b88da6520b69dcbb49066a66` |
